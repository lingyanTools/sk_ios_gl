//
//  SceneDelegate.h
//  sk_ios_gl
//
//  Created by 灵岩 on 2024/2/1.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

