#import "ViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self glkInit];
    [self timerInit];
    NSLog(@"view controller load");
}

-(void)glkInit
{
    // 获取当前窗体
    UIWindow* window = [[UIApplication sharedApplication] windows].firstObject;
    // 获取显示的范围
    CGRect bound = CGRectMake(0, window.safeAreaInsets.top, self.view.frame.size.width, self.view.frame.size.height-window.safeAreaInsets.top);
    // 初始化GLKView
    _glkView = [[GLKView alloc] initWithFrame:bound];
    // 初始化EAGLContext上下文
    _glkView.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    // 设置代理
    _glkView.delegate = self;
    _glkView.enableSetNeedsDisplay = NO;
    // 添加glkView到当前视图
    [[self view] addSubview:_glkView];
}

-(void)timerInit
{
    if(!_displayLink)
    {
        _displayLink  = [CADisplayLink displayLinkWithTarget:self selector:@selector(render:)];
        [_displayLink setPreferredFramesPerSecond:60];
        [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode: NSRunLoopCommonModes];
    }
}

- (void)render:(CADisplayLink *)displayLink
{
    [_glkView display];
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClearColor(1, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
    NSLog(@"aaa");
}
@end
