//
//  ViewController.h
//  sk_ios_gl
//
//  Created by 灵岩 on 2024/2/1.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
@interface ViewController : UIViewController<GLKViewDelegate>
@property (nonatomic, strong) GLKView           * glkView;
@property (nonatomic, strong) CADisplayLink     * displayLink;
-(void)glkInit;
@end

